/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package ristgo

import (
	"context"
	"unsafe"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

//ReceiverConfig is the configuration used by ReceiverCreate
type SenderConfig struct {
	//RistProfile RIST profile to use for this session
	RistProfile libristwrapper.RistProfile
	//LoggingCallbackFunction is an optional logging callback function
	LoggingCallbackFunction libristwrapper.LogCallbackFunc
	//StatsCallbackFunction is an optional stats callback function
	StatsCallbackFunction libristwrapper.StatsCallbackFunc
	//StatsInterval is the interval in ms at which the StatsCallbackFunction will be called
	StatsInterval int
	//RecoveryBufferSize is the size in ms of the recovery buffer
	RecoveryBufferSize int
	//VirtDstPort is the tunneled destination port (main profile only)
	VirtDstPort uint16
	//VirtSrcPort is the tunneled source port (main profile only)
	VirtSrcPort uint16
}

//CreateSender creates a RIST sender
func CreateSender(c context.Context, config *SenderConfig) (*Sender, error) {
	var (
		err             error                               = nil
		ristCtx         libristwrapper.RistCtx              = nil
		loggingSettings *libristwrapper.RistLoggingSettings = nil
	)

	if config.LoggingCallbackFunction != nil {
		loggingSettings = libristwrapper.CreateRistLoggingSettingsWithCB(config.LoggingCallbackFunction)
	}
	defer func() {
		if err != nil {
			libristwrapper.CleanupLoggingSettings(loggingSettings)
		}
	}()

	ristCtx, err = libristwrapper.SenderCreate(config.RistProfile, loggingSettings)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			libristwrapper.RistDestroy(ristCtx)
		}
	}()

	var statsCBPtr unsafe.Pointer = nil
	if config.StatsCallbackFunction != nil {
		statsCBPtr, err = libristwrapper.SetStatsCallback(ristCtx, config.StatsInterval, config.StatsCallbackFunction)
		if err != nil {
			return nil, err
		}
	}

	senderContext, cancelFunc := context.WithCancel(c)
	s := &Sender{
		context:            senderContext,
		cancel:             cancelFunc,
		loggingSettings:    loggingSettings,
		statsCBPtr:         statsCBPtr,
		recoveryBufferSize: config.RecoveryBufferSize,
		logCB:              config.LoggingCallbackFunction,
		virt_dst_port:      config.VirtDstPort,
		virt_src_port:      config.VirtSrcPort,
	}
	go s.waitCancel()
	return s, nil
}

type Sender struct {
	context            context.Context
	cancel             context.CancelFunc
	ristCtx            libristwrapper.RistCtx
	loggingSettings    *libristwrapper.RistLoggingSettings
	statsCBPtr         unsafe.Pointer
	recoveryBufferSize int
	peers              peers
	logCB              libristwrapper.LogCallbackFunc
	virt_dst_port      uint16
	virt_src_port      uint16
}

//Addpeer to Sender
func (s *Sender) AddPeer(config *PeerConfig) (int, error) {
	return s.peers.addPeer(s.ristCtx, config, uint32(s.recoveryBufferSize))
}

//RemovePeer from sender
func (s *Sender) RemovePeer(id int) error {
	return s.peers.removePeer(id)
}

//Write data out to connected peers
func (s *Sender) Write(buf []byte) (n int, err error) {
	return libristwrapper.SenderSendData(s.ristCtx, buf, s.virt_src_port, s.virt_dst_port)
}

//Close all connections and cleanup associated resources
func (s *Sender) Close() {
	s.cancel()
}

func (s *Sender) waitCancel() {
	<-s.context.Done()
	libristwrapper.RistDestroy(s.ristCtx)
}
