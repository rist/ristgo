/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"context"
	"flag"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"

	"code.videolan.org/rist/ristgo"
)

var gRistreceiver ristgo.Receiver = nil
var gRistrecoverysize = 1000
var gStatsIntervalSeconds = 10
var receiverflows map[uint16]*receiveflow = make(map[uint16]*receiveflow)

var ctx context.Context

type arrayFlags []string

func (i *arrayFlags) String() string {
	return ""
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

func WaitForCtrlC() {
	var end_waiter sync.WaitGroup
	end_waiter.Add(1)
	signal_channel := make(chan os.Signal, 1)
	signal.Notify(signal_channel, os.Interrupt)
	go func() {
		<-signal_channel
		end_waiter.Done()
	}()
	end_waiter.Wait()
}

func setupInputs(inputs arrayFlags) error {
	for _, i := range inputs {
		inputurl, err := url.Parse(i)
		if err != nil {
			return fmt.Errorf("couldn't parse input url %s: %w", i, err)
		}
		switch inputurl.Scheme {
		case "rist":
			err := setupRistInput(inputurl)
			if err != nil {
				return err
			}
		default:
			return fmt.Errorf("input url scheme: %s not implemented", inputurl.Scheme)
		}
	}
	return nil
}

func main() {
	var inputs arrayFlags
	var outputs arrayFlags
	var (
		statsfile      string
		influxdburl    string
		influxdbtoken  string
		influxdborg    string
		influxdbbucket string
	)
	flag.Var(&inputs, "input", "input url, multiple instances of -input may be defined, with a minimum of 1")
	flag.Var(&outputs, "output", "output url, multiple instances of -output may be defined, with a minimum of 1")
	flag.IntVar(&gRistrecoverysize, "rist-recoverysize", 1000, "recovery buffer size in ms")
	flag.IntVar(&gStatsIntervalSeconds, "stats-interval", 10, "stats reporting interval in seconds")
	flag.StringVar(&statsfile, "stats-file", "", "base name for stats file")
	flag.StringVar(&influxdburl, "influxdb-url", "", "url of influxdb server to write stats to")
	flag.StringVar(&influxdbtoken, "influxdb-token", "", "influxdb token")
	flag.StringVar(&influxdborg, "influxdb-org", "", "influxdb org")
	flag.StringVar(&influxdbbucket, "influxdb-bucket", "", "influxdb bucket")
	flag.StringVar(&influxDBIdentifier, "influxdb-identifier", "", "identifier used in influxdb as identifier tag")
	flag.BoolVar(&gStatsStdout, "stats-stdout", false, "print stats to stdout")
	flag.Parse()

	c := context.Background()
	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(c)
	if len(inputs) < 1 || len(outputs) < 1 {
		flag.Usage()
		os.Exit(1)
	}
	err := setupInputs(inputs)
	if err != nil {
		LogMsg("PANIC", "error: %s setting up inputs", err)

	}
	err = setupOutputs(outputs)
	if err != nil {
		LogMsg("PANIC", "error: %s setting up outputs", err)
	}

	if statsfile != "" {
		err := SetupStatsFile(statsfile)
		if err != nil {
			LogMsg("PANIC", "error: %s setting up statsfile", err)

		}
	}
	if influxdburl != "" {
		SetupInfluxDB(influxdburl, influxdbtoken, influxdbbucket, influxdborg)
	}
	WaitForCtrlC()

	gRistreceiver.Destroy()
	cancel()
}
