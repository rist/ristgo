/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"io"

	"code.videolan.org/rist/ristgo"
)

type receiveflow struct {
	flow         ristgo.ReceiverFlow
	outputs      map[int]*Output
	outPutAdd    chan io.Writer
	outPutRemove chan io.Writer
	outRemoveIdx chan int
}

func CreateReceiverFlow(destinationPort uint16) *receiveflow {
	rf := &receiveflow{
		outputs:      make(map[int]*Output),
		outPutAdd:    make(chan io.Writer, 4),
		outPutRemove: make(chan io.Writer, 4),
		outRemoveIdx: make(chan int, 16),
		flow:         nil,
	}
	receiverflows[destinationPort] = rf
	go receiveLoop(rf)
	return rf
}

func receiveLoop(rf *receiveflow) {
	var err error
	rf.flow, err = gRistreceiver.ConfigureFlow(0)
	outputidx := 0
	if err != nil {
		panic(err)
	}
main:
	for {
		select {
		case rb := <-rf.flow.DataChannel():
			rf.writeOutputs(rb)
		case output := <-rf.outPutAdd:
			rf.addOutput(output, outputidx)
			outputidx++
		case idx := <-rf.outRemoveIdx:
			delete(rf.outputs, idx)
		case output := <-rf.outPutRemove:
			for idx, o := range rf.outputs {
				if o.w == output {
					delete(rf.outputs, idx)
					break
				}
			}
			LogMsg("ERROR", "output not found in map")
		case <-ctx.Done():
			break main
		}
	}
}
