/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"net/url"

	"code.videolan.org/rist/ristgo"
	"code.videolan.org/rist/ristgo/libristwrapper"
)

func init() {
	globalLogCallback := ristgo.CreateDefaultLoggingCallback(libristwrapper.LogLevelInfo)
	libristwrapper.SetGlobalLoggingCB(globalLogCallback)
}

func statsCallback(stats *libristwrapper.StatsContainer) {
	if stats.ReceiverFlowStats != nil {
		go HandleStats("", "", nil, stats.ReceiverFlowStats)
	} else if stats.SenderStats != nil {
		go HandleStats("", "", nil, stats.SenderStats)
	}
}

func setupRistInput(u *url.URL) error {
	if gRistreceiver == nil {
		var err error
		gRistreceiver, err = ristgo.ReceiverCreate(ctx, &ristgo.ReceiverConfig{
			RistProfile:             libristwrapper.RistProfileSimple,
			LoggingCallbackFunction: ristgo.CreateDefaultLoggingCallback(libristwrapper.LogLevelInfo),
			StatsCallbackFunction:   statsCallback,
			StatsInterval:           gStatsIntervalSeconds * 1000,
			RecoveryBufferSize:      gRistrecoverysize,
		})
		if err != nil {
			return err
		}
		gRistreceiver.Start()
	}
	peerConfig, err := ristgo.ParseRistURL(u)
	if err != nil {
		return err
	}
	gRistreceiver.AddPeer(peerConfig)
	return nil
}
