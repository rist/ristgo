/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"syscall"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

type Output struct {
	w    io.Writer
	i    int
	rf   *receiveflow
	data chan []byte
}

func (rf *receiveflow) addOutput(w io.Writer, i int) {
	rf.outputs[i] = &Output{
		w,
		i,
		rf,
		make(chan []byte, 128),
	}
	//go rf.outputs[i].RunLoop()
}

func (o *Output) RunLoop() {
main:
	for {
		select {
		case <-ctx.Done():
			break main
		default:
			//
		}
		buf := <-o.data
		err := o.writeOut(buf)
		if err != nil {
			return
		}
	}
}

func (o *Output) writeOut(buf []byte) error {
	_, err := o.w.Write(buf)
	if err == nil {
		return nil
	}
	switch e := err.(type) {
	case *net.OpError:
		eu := e.Unwrap()
		if esyscall, ok := eu.(*os.SyscallError); ok {
			errno := int(esyscall.Err.(syscall.Errno))
			if errno == 111 {
				return nil
			}
		}
	}
	LogMsg("PANIC", "unhandled error while writing to output: %s", err)
	return nil
}

func (rf *receiveflow) writeOutputs(rb *libristwrapper.RistDataBlock) {
	if len(rb.Data) == 0 {
		return
	}
	for _, out := range rf.outputs {
		out.writeOut(rb.Data)
	}
	rb.Return()
}

func setupOutputs(outputs arrayFlags) error {
	for _, o := range outputs {
		destinationPort := uint16(0)
		rf, ok := receiverflows[destinationPort]
		if !ok {
			rf = CreateReceiverFlow(destinationPort)
		}
		outputurl, err := url.Parse(o)
		if err != nil {
			return fmt.Errorf("couldn't parse output url %s: %w", o, err)
		}
		var outPut io.Writer = nil
		switch outputurl.Scheme {
		case "udp":
			outPut, err = parseUdpOutputUrl(outputurl)
			if err != nil {
				return fmt.Errorf("couldn't setup udp output %s: %w", outputurl, err)
			}
		default:
			return fmt.Errorf("output url scheme: %s not implemented", outputurl.Scheme)
		}
		if outPut != nil {
			rf.outPutAdd <- outPut
		}
	}
	return nil
}
