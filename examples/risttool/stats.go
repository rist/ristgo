/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"reflect"
	"time"

	"code.videolan.org/rist/ristgo/libristwrapper"
	"github.com/Showmax/go-fqdn"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
)

var (
	gStatsStdout                              = false
	statsFile          *rotatelogs.RotateLogs = nil
	influxDBWriteApi   api.WriteAPIBlocking   = nil
	influxDBIdentifier string
	hostname           string
)

type StatsPrepend struct {
	Timestamp string
	Type      string
	Host      string
}

type WrappedRistReceiverFlowStats struct {
	*StatsPrepend
	*libristwrapper.ReceiverFlowStats
}

type WrappedRistSenderStats struct {
	*StatsPrepend
	*libristwrapper.SenderPeerStats
}

func SetupStatsFile(file string) error {
	var err error
	statsFilePattern := file + ".%Y%m%d"
	statsFile, err = rotatelogs.New(
		statsFilePattern,
		rotatelogs.WithClock(rotatelogs.Local),
		rotatelogs.WithLinkName(file),
	)
	if err != nil {
		return err
	}
	return nil
}

func SetupInfluxDB(url, token, bucket, org string) {
	var err error
	client := influxdb2.NewClient(url, token)
	influxDBWriteApi = client.WriteAPIBlocking(org, bucket)
	hostname, err = fqdn.FqdnHostname()
	if err != nil {
		panic(err)
	}
}

func structToMap(s interface{}) map[string]interface{} {
	m := make(map[string]interface{})
	elem := reflect.ValueOf(s).Elem()
	relType := elem.Type()
	for i := 0; i < relType.NumField(); i++ {
		m[relType.Field(i).Name] = elem.Field(i).Interface()
	}
	return m
}

func WriteInfluxStats(host, identifier string, u *url.URL, stats interface{}) {
	values := structToMap(stats)
	var (
		measurement string
		cname       string
	)
	switch stats.(type) {
	case *libristwrapper.ReceiverFlowStats:
		measurement = "rist-receive"
		cname = values["CName"].(string)
		delete(values, "CName")
	case *libristwrapper.SenderPeerStats:
		measurement = "rist-sender"
		cname = values["CName"].(string)
	default:
		panic("wrong interface")
	}
	tags := map[string]string{"identifier": influxDBIdentifier, "hostname": hostname, "remotehost": host}
	if u != nil {
		tags["localurl"] = u.Host
	}
	if identifier != "" {
		tags["output_identifier"] = identifier
	}
	if cname != "" {
		tags["cname"] = cname
	}
	point := influxdb2.NewPoint(
		measurement,
		tags,
		values,
		time.Now(),
	)
	err := influxDBWriteApi.WritePoint(context.Background(), point)
	if err != nil {
		LogMsg("PANIC", "error writing to influxdb: %s", err)
	}
}

func HandleStats(Host, identifier string, u *url.URL, stats interface{}) {
	now := time.Now()
	prepend := &StatsPrepend{now.Format("2006-01-02T15:04:05-0700"), "", Host}
	var wrappedStats interface{}
	switch v := stats.(type) {
	case *libristwrapper.ReceiverFlowStats:
		prepend.Type = "RistReceiverStats"
		wrappedStats = &WrappedRistReceiverFlowStats{prepend, v}
	case *libristwrapper.SenderPeerStats:
		prepend.Type = "RistSenderStats"
		wrappedStats = &WrappedRistSenderStats{prepend, v}
	}
	statsJson, err := json.Marshal(wrappedStats)
	statsString := string(statsJson) + "\n"
	if err != nil {
		panic(err)
	}
	if gStatsStdout {
		fmt.Print(statsString)
	}
	if statsFile != nil {
		statsFile.Write([]byte(statsString))
	}
	if influxDBWriteApi != nil {
		go WriteInfluxStats(Host, identifier, u, stats)
	}
}
