/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"net"
	"net/url"
)

func parseUdpOutputUrl(u *url.URL) (*net.UDPConn, error) {
	mcastIface := u.Query().Get("iface")
	var sourceIP *net.UDPAddr = nil
	if mcastIface != "" {
		if iface, err := net.InterfaceByName(mcastIface); err == nil {
			addrs, err := iface.Addrs()
			if err != nil {
				return nil, err
			}
			ipnet := addrs[0].(*net.IPNet)
			ip := ipnet.IP
			sourceIP, err = net.ResolveUDPAddr("udp", ip.String()+":0")
			if err != nil {
				return nil, err
			}
		} else if sourceIP, err = net.ResolveUDPAddr("udp", mcastIface+":0"); err != nil {
			return nil, err
		}
	}
	target, err := net.ResolveUDPAddr("udp", u.Host)
	if err != nil {
		return nil, err
	}
	return net.DialUDP("udp", sourceIP, target)
}
