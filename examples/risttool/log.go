/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package main

import (
	"fmt"
	"strings"
	"time"
)

func LogMsg(level string, format string, arg ...interface{}) {
	timeNow := time.Now()
	msg := fmt.Sprintf(format, arg...)
	fmt.Printf("[%s] [%s]: %s\n", timeNow.Format("2006-01-02T15:04:05-0700"), level, strings.TrimSuffix(msg, "\n"))
	if level == "PANIC" {
		panic("exit")
	}
}
