/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package ristgo

import (
	"fmt"
	"net"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

//PeerConfig configuration for a libRIST peer
type PeerConfig struct {
	Address    string //Address is the address in the form: rist://@1.2.3.4:5678 where @ indicates listening
	Interface  string //Interface: optional interface which to use for multicast reception
	Psk        string //Psk: optional PSK secret
	PskKeySize int    //PskKeySize: optional PSK keysize (128 or 256)
	CName      string //CName: optional cname to be transmitted on this connection
}

//ParseRistURL helper function that parses a url.URL object to PeerConfig
func ParseRistURL(u *url.URL) (*PeerConfig, error) {
	var peerConfig PeerConfig
	_, err := net.ResolveUDPAddr("udp", u.Host)
	if err != nil {
		return nil, err
	}
	tmp := strings.Split(u.String(), "?")
	peerConfig.Address = tmp[0]

	peerConfig.Interface = u.Query().Get("iface")
	peerConfig.CName = u.Query().Get("cname")
	peerConfig.Psk = u.Query().Get("psk")
	aesSize := u.Query().Get("aes-key-size")
	peerConfig.PskKeySize = 0
	if aesSize != "" {
		keySize, err := strconv.Atoi(aesSize)
		if err != nil {
			return nil, err
		}
		switch keySize {
		case 128:
			peerConfig.PskKeySize = 128
		case 256:
			peerConfig.PskKeySize = 256
		default:
			return nil, fmt.Errorf("keysize %d is invalid", keySize)
		}
	} else if peerConfig.Psk != "" {
		peerConfig.PskKeySize = 256
	}
	return &peerConfig, nil
}

type peers struct {
	peersLock  sync.Mutex
	lastPeerId int
	peers      map[int]libristwrapper.Peer
}

func (p *peers) addPeer(ctx libristwrapper.RistCtx, config *PeerConfig, recoveryBufferSize uint32) (int, error) {
	peerConfig := libristwrapper.PeerConfig{
		Address:        config.Address,
		Interface:      config.Interface,
		Psk:            config.Psk,
		PskKeySize:     config.PskKeySize,
		CName:          config.CName,
		RecoveryBuffer: recoveryBufferSize,
	}
	peer, err := libristwrapper.CreatePeer(ctx, peerConfig)
	if err != nil {
		return -1, err
	}

	p.peersLock.Lock()
	defer p.peersLock.Unlock()
	if p.peers == nil {
		p.peers = make(map[int]libristwrapper.Peer)
	}
	id := p.lastPeerId
	p.lastPeerId++
	if _, ok := p.peers[id]; ok {
		panic("peer already exists at index, this shouldn't happen")
	}
	p.peers[id] = peer
	return id, nil
}

func (p *peers) removePeer(id int) error {
	p.peersLock.Lock()
	defer p.peersLock.Unlock()
	if p.peers == nil {
		p.peers = make(map[int]libristwrapper.Peer)
	}
	if peer, ok := p.peers[id]; ok {
		err := peer.Destroy()
		delete(p.peers, id)
		return err
	}
	return fmt.Errorf("error deleting peer: peer not found")
}
