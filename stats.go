/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package ristgo

import (
	"encoding/json"
	"fmt"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

//CreateDefaultStatsCallback creates a callback that will print stats in json format to stdout
func CreateDefaultStatsCallback() libristwrapper.StatsCallbackFunc {
	return func(s *libristwrapper.StatsContainer) {
		var (
			stats []byte
			err   error
		)
		if s.ReceiverFlowStats != nil {
			stats, err = json.Marshal(s.ReceiverFlowStats)
			if err != nil {
				fmt.Printf("failure marshalling stats to json: %s", err)
				return
			}
		} else if s.SenderStats != nil {
			stats, err = json.Marshal(s.SenderStats)
			if err != nil {
				fmt.Printf("failure marshalling stats to json: %s", err)
				return
			}
		} else {
			return
		}
		stats = stats[:0:0]
		fmt.Println(string(stats))
	}
}
