/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>
#include <string.h>
*/
import "C"

import (
	"errors"
	"fmt"
	"unsafe"

	"golang.org/x/sys/unix"
)

//PeerConfig configuration for a libRIST peer
type PeerConfig struct {
	Address        string //Address is the address in the form: rist://@1.2.3.4:5678 where @ indicates listening
	Interface      string //Interface: optional interface which to use for multicast reception
	Psk            string //Psk: optional PSK secret
	PskKeySize     int    //PskKeySize: optional PSK keysize (128 or 256)
	CName          string //CName: optional cname to be transmitted on this connection
	RecoveryBuffer uint32 //RecoveryBuffer size in ms
}

//Peer is a libRIST connection endpoint
type Peer interface {
	Destroy() error //Destroy Peer
}

//CreatePeer creates a new Peer
func CreatePeer(ctx RistCtx, config PeerConfig) (Peer, error) {
	peerConfig, err := createPeerConfig(config)
	if err != nil {
		return nil, fmt.Errorf("failed creating peer: %w", err)
	}

	var ristPeer *C.struct_rist_peer
	if res := C.rist_peer_create(ctx, &ristPeer, peerConfig); res != 0 {
		return nil, fmt.Errorf("failed to create peer, error code: %d", res)
	}

	return &peer{
		ctx:  ctx,
		peer: ristPeer,
	}, nil
}

type peer struct {
	ctx  RistCtx
	peer *C.struct_rist_peer
}

func createPeerConfig(config PeerConfig) (*C.struct_rist_peer_config, error) {
	var peerConfig C.struct_rist_peer_config
	if res := C.rist_peer_config_defaults_set(&peerConfig); res != 0 {
		return nil, fmt.Errorf("could not set peer defaults")
	}

	addr, err := unix.ByteSliceFromString(config.Address)
	if err != nil {
		return nil, fmt.Errorf("could not convert address to byteslice: %w", err)
	}
	if len(addr) > C.RIST_MAX_STRING_LONG {
		return nil, errors.New("address exceeds max length")
	}
	C.memcpy(unsafe.Pointer(&peerConfig.address[0]), unsafe.Pointer(&addr[0]), C.RIST_MAX_STRING_LONG)

	miface, err := unix.ByteSliceFromString(config.Interface)
	if err != nil {
		return nil, fmt.Errorf("could not convert interface to byteslice: %w", err)
	}
	if len(miface) > C.RIST_MAX_STRING_SHORT {
		return nil, errors.New("interface exceeds max length")
	}
	C.memcpy(unsafe.Pointer(&peerConfig.miface[0]), unsafe.Pointer(&miface[0]), C.RIST_MAX_STRING_SHORT)

	if config.Psk != "" {
		peerConfig.key_size = C.int(config.PskKeySize)
		//The spec says we are to default to AES-256 (2)
		if config.PskKeySize == 0 {
			peerConfig.key_size = 2
		}
		secret, err := unix.ByteSliceFromString(config.Psk)
		if err != nil {
			return nil, err
		}
		if len(secret) > C.RIST_MAX_STRING_SHORT {
			return nil, errors.New("psk exceeds max length")
		}
		C.memcpy(unsafe.Pointer(&peerConfig.secret[0]), unsafe.Pointer(&secret[0]), C.RIST_MAX_STRING_SHORT)
	}

	cname, err := unix.ByteSliceFromString(config.CName)
	if err != nil {
		return nil, err
	}
	if len(cname) > C.RIST_MAX_STRING_SHORT {
		return nil, errors.New("cname exceeds max length")
	}
	C.memcpy(unsafe.Pointer(&peerConfig.cname[0]), unsafe.Pointer(&cname[0]), C.RIST_MAX_STRING_SHORT)

	peerConfig.recovery_length_max = C.uint(config.RecoveryBuffer)
	peerConfig.recovery_length_min = C.uint(config.RecoveryBuffer)
	peerConfig.recovery_rtt_min = 1
	peerConfig.recovery_rtt_max = 1000

	return &peerConfig, nil
}

func (p *peer) Destroy() error {
	if res := C.rist_peer_destroy(p.ctx, p.peer); res != 0 {
		return fmt.Errorf("failed to destroy peer, error code: %d", res)
	}
	return nil
}
