/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

type RistDataReadTimeout struct {
}

func (e *RistDataReadTimeout) Error() string {
	return "timeout exceeded while reading from rist receiver"
}
