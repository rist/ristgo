/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>

extern int ristDataCallback(void *arg, struct rist_data_block *data_block);

*/
import "C"

import (
	"fmt"
	"unsafe"

	gopointer "github.com/mattn/go-pointer"
)

type RistDataCallBackFunc func(*RistDataBlock)

//export ristDataCallbackWrapper
func ristDataCallbackWrapper(ptr unsafe.Pointer, data_block *C.struct_rist_data_block) {
	block := wrapLibRistDataBlock(data_block)
	cb := gopointer.Restore(ptr).(RistDataCallBackFunc)
	cb(block)
}

func RistSetDataCallback(ctx RistCtx, fn RistDataCallBackFunc) (unsafe.Pointer, error) {
	ptr := gopointer.Save(fn)
	if res := C.rist_receiver_data_callback_set2(ctx, (C.receiver_data_callback2_t)(C.ristDataCallback), ptr); res != 0 {
		gopointer.Unref(ptr)
		return nil, fmt.Errorf("failed to set data callback %d", res)
	}
	return ptr, nil
}

func RistUnsetDataCallback(ctx RistCtx, ptr unsafe.Pointer) {
	C.rist_receiver_data_callback_set2(ctx, nil, nil)
	gopointer.Unref(ptr)
}
