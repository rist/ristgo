/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>
*/
import "C"
import "fmt"

//RistCtx libRIST instance context
type RistCtx *C.struct_rist_ctx

func RistDestroy(ctx RistCtx) error {
	res := C.rist_destroy(ctx)
	if res != 0 {
		return fmt.Errorf("error destroying rist receiver: %d", res)
	}
	return nil
}
