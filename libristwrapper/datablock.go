package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>
*/
import "C"
import (
	"reflect"
	"runtime"
	"sync"
	"sync/atomic"
	"unsafe"
)

type libRistDataBlock *C.struct_rist_data_block

type RistDataBlock struct {
	Data          []byte
	RtpTimestamp  uint32
	TimeStamp     int64
	SrcPort       uint16
	DstPort       uint16
	FlowID        uint32
	SeqNo         uint32
	ExtendedSeq   bool
	Discontinuity bool
	b             libRistDataBlock
	refCnt        *uint32
}

func (b *RistDataBlock) Return() {
	b.decrement()
}

func (b *RistDataBlock) Increment() {
	atomic.AddUint32(b.refCnt, 1)
}

func (b *RistDataBlock) decrement() {
	if atomic.AddUint32(b.refCnt, ^uint32(0)) == 0 {
		ristDataBlockFreeFunc(b)
		runtime.SetFinalizer(b, nil)
		ristDataBlockPool.Put(b)
	}
}

var ristDataBlockPool = sync.Pool{
	New: func() interface{} {
		return &RistDataBlock{
			refCnt: new(uint32),
		}
	},
}

func ristDataBlockFreeFunc(b *RistDataBlock) {
	//Free the underlying data
	C.rist_receiver_data_block_free2((**C.struct_rist_data_block)(&b.b))
	//Make sure there are no dangling pointers to underlying data
	b.b = nil
	h := (*reflect.SliceHeader)(unsafe.Pointer(&b.Data))
	h.Data = 0
	h.Len = 0
	h.Cap = 0
}

func wrapLibRistDataBlock(b  *C.struct_rist_data_block) *RistDataBlock {
	block := ristDataBlockPool.Get().(*RistDataBlock)
	block.RtpTimestamp = uint32(b.ts_ntp)
	block.TimeStamp = int64(b.ts_ntp)
	block.SrcPort = uint16(b.virt_src_port)
	block.DstPort = uint16(b.virt_dst_port)
	block.FlowID = uint32(b.flow_id)
	block.SeqNo = uint32(b.seq)
	block.ExtendedSeq = false
	block.Discontinuity = false
	if b.flags&C.RIST_DATA_FLAGS_DISCONTINUITY != 0 {
		block.Discontinuity = true
	}
	h := (*reflect.SliceHeader)(unsafe.Pointer(&block.Data))
	h.Data = uintptr(b.payload)
	h.Len = int(b.payload_len)
	h.Cap = int(b.payload_len)
	block.b = b
	runtime.SetFinalizer(block, ristDataBlockFreeFunc)
	*block.refCnt = 1
	return block
}
