/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

import (
	"errors"
	"fmt"
	"unsafe"
)

/*
 #cgo pkg-config: librist
 #include <librist/librist.h>
*/
import "C"

func SenderCreate(profile RistProfile, loggingSettings *RistLoggingSettings) (RistCtx, error) {
	var ctx *C.struct_rist_ctx
	if res := C.rist_sender_create(&ctx, (C.enum_rist_profile)(profile), 0, (*C.struct_rist_logging_settings)(loggingSettings)); res != 0 {
		return nil, fmt.Errorf("error creating rist sender, error code: %d", res)
	}
	return RistCtx(ctx), nil
}

func SenderSendData(ctx RistCtx, data []byte, virt_src_port uint16, virt_dst_port uint16) (int, error) {
	var block C.struct_rist_data_block
	block.payload = unsafe.Pointer(&data[0])
	block.payload_len = C.size_t(len(data))
	block.virt_dst_port = C.uint16_t(virt_dst_port)
	block.virt_src_port = C.uint16_t(virt_src_port)
	n := int(C.rist_sender_data_write(ctx, &block))
	if n < len(data) {
		return n, errors.New("librist: error sending data")
	}
	return n, nil
}
