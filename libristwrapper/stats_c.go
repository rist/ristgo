/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#include <librist/librist.h>

extern int ristStatsCBWrapper(void*, const struct rist_stats*);

int ristStatsCB(void* arg, const struct rist_stats* stats_container)
{
	ristStatsCBWrapper(arg, stats_container);
	return 0;
}

*/
import "C"
