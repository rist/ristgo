/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#include <librist/librist.h>

extern int ristLogCBWrapper(void*, enum rist_log_level, char*);

int ristLogCB(void* arg, enum rist_log_level level, char* message)
{
	ristLogCBWrapper(arg, level, message);
	return 0;
}

*/
import "C"
