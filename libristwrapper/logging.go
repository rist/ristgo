/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>
#include <librist/logging.h>
#include <stdlib.h>
extern int ristLogCB(void* arg, enum rist_log_level level, char* message);
*/
import "C"

import (
	"errors"
	"sync"
	"unsafe"

	gopointer "github.com/mattn/go-pointer"
)

func init() {
	//we need to make sure the global logging CB is disabled, even if
	//we are to set it later, otherwise if users would create a ctx
	//without it global logging being set their logSettings would be used
	//globally as well.
	e := DisableGlobalLoggingCB()
	if e != nil {
		panic(e)
	}
}

//RistLogLevel severity of log messages
type RistLogLevel int

//Available log levels
const (
	LogLevelDisable RistLogLevel = RistLogLevel(logLevelDisable)
	LogLevelError   RistLogLevel = RistLogLevel(logLevelError)
	LogLevelWarn    RistLogLevel = RistLogLevel(logLevelWarn)
	LogLevelNotice  RistLogLevel = RistLogLevel(logLevelNotice)
	LogLevelInfo    RistLogLevel = RistLogLevel(logLevelInfo)
	LogLevelDebug   RistLogLevel = RistLogLevel(logLevelDebug)
)

const (
	logLevelDisable = C.enum_rist_log_level(C.RIST_LOG_DISABLE)
	logLevelError   = C.enum_rist_log_level(C.RIST_LOG_ERROR)
	logLevelWarn    = C.enum_rist_log_level(C.RIST_LOG_WARN)
	logLevelNotice  = C.enum_rist_log_level(C.RIST_LOG_NOTICE)
	logLevelInfo    = C.enum_rist_log_level(C.RIST_LOG_INFO)
	logLevelDebug   = C.enum_rist_log_level(C.RIST_LOG_DEBUG)
)

//RistLoggingSettings rist logging configuration struct
type RistLoggingSettings C.struct_rist_logging_settings

//LogCallbackFunc is an optionally set callback function/closure for logmessages
type LogCallbackFunc func(loglevel RistLogLevel, logmessage string)

var (
	globalLogCBLock sync.Mutex
	globalLogCBPtr  unsafe.Pointer = nil
)

//export ristLogCBWrapper
func ristLogCBWrapper(arg unsafe.Pointer, level C.enum_rist_log_level, msg *C.char) {
	userCb := gopointer.Restore(arg).(LogCallbackFunc)
	go userCb(RistLogLevel(level), C.GoString(msg))
}

//SetGlobalLoggingCB sets the LogCallbackFunc for the global libRIST logger
func SetGlobalLoggingCB(cb LogCallbackFunc) error {
	ptr := gopointer.Save(cb)
	var logSettings RistLoggingSettings
	logSettingsDefaultInit(&logSettings)
	logSettings.log_cb = (*[0]byte)(C.ristLogCB)
	logSettings.log_cb_arg = ptr
	if res := C.rist_logging_set_global((*C.struct_rist_logging_settings)(&logSettings)); res != 0 {
		gopointer.Unref(ptr)
		return errors.New("unable to set global logging")
	}
	storeGlobalLoggingPtr(ptr)
	return nil
}

func storeGlobalLoggingPtr(ptr unsafe.Pointer) {
	globalLogCBLock.Lock()
	defer globalLogCBLock.Unlock()
	if globalLogCBPtr != nil {
		gopointer.Unref(globalLogCBPtr)
	}
	globalLogCBPtr = ptr

}

//DisableGlobalLoggingCB disables global logging
func DisableGlobalLoggingCB() error {
	var logSettings RistLoggingSettings
	logSettingsDefaultInit(&logSettings)
	if res := C.rist_logging_set_global((*C.struct_rist_logging_settings)(&logSettings)); res != 0 {
		return errors.New("unable to disable global logging")
	}
	storeGlobalLoggingPtr(nil)
	return nil
}

//CreateRistLoggingSettingsWithCB creates RistLoggingSettings configured to use cb
func CreateRistLoggingSettingsWithCB(cb LogCallbackFunc) *RistLoggingSettings {
	ptr := gopointer.Save(cb)
	//we need to malloc here because libRIST would store the pointer
	logSettings := (*RistLoggingSettings)(C.malloc(C.size_t(unsafe.Sizeof(C.struct_rist_logging_settings{}))))
	logSettingsDefaultInit(logSettings)
	logSettings.log_cb = (*[0]byte)(C.ristLogCB)
	logSettings.log_cb_arg = ptr
	return logSettings
}

//CleanupLoggingSettings cleans up any configured cb's used in RistLoggingSettings
func CleanupLoggingSettings(logSettings *RistLoggingSettings) {
	if logSettings != nil && logSettings.log_cb_arg != nil {
		ptr := logSettings.log_cb_arg
		gopointer.Unref(ptr)
		C.free(unsafe.Pointer(logSettings))
	}
}

func logSettingsDefaultInit(logSettings *RistLoggingSettings) {
	logSettings.log_level = logLevelDebug
	logSettings.log_cb = nil
	logSettings.log_cb_arg = nil
	logSettings.log_stream = nil
	logSettings.log_socket = -1
}
