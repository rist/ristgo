/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>

extern int ristStatsCB(void* arg, const struct rist_stats* stats_container);
*/
import "C"

import (
	"fmt"
	"unsafe"

	gopointer "github.com/mattn/go-pointer"
)

type SenderPeerStats struct {
	CName                string
	PeerId               uint32
	Bandwidth            uint
	RetryBandwidth       uint
	SentPackets          uint64
	ReceivedPackets      uint64
	RetransmittedPackets uint64
	Quality              float64
	Rtt                  uint32
}

type ReceiverFlowStats struct {
	PeerCount                 uint32
	CName                     string
	FlowID                    uint32
	Status                    int
	Bandwidth                 uint
	RetryBandwidth            uint
	SentPackets               uint64
	ReceivedPackets           uint64
	MissingPackets            uint32
	ReorderedPackets          uint32
	RecoveredPackets          uint32
	RecoveredFirstRetryReq    uint32
	LostPackets               uint32
	Quality                   float64
	MinimalInterPacketArrival uint64
	CurrentInterPacketArrival uint64
	MaximumInterPacketArrival uint64
	Rtt                       uint32
}

type StatsContainer struct {
	SenderStats       *SenderPeerStats   `json:"senderstats,omitempty"`
	ReceiverFlowStats *ReceiverFlowStats `json:"receiverstats,omitempty"`
}

type StatsCallbackFunc func(*StatsContainer)

//export ristStatsCBWrapper
func ristStatsCBWrapper(arg unsafe.Pointer, stats_container *C.struct_rist_stats) {
	defer C.rist_stats_free(stats_container)
	var statsContainer StatsContainer
	if stats_container.stats_type == C.RIST_STATS_SENDER_PEER {
		var senderStats = (*C.struct_rist_stats_sender_peer)(unsafe.Pointer(&stats_container.stats[0]))
		statsContainer.SenderStats = &SenderPeerStats{
			CName:                C.GoString(&senderStats.cname[0]),
			PeerId:               uint32(senderStats.peer_id),
			Bandwidth:            uint(senderStats.bandwidth),
			RetryBandwidth:       uint(senderStats.retry_bandwidth),
			SentPackets:          uint64(senderStats.sent),
			ReceivedPackets:      uint64(senderStats.received),
			RetransmittedPackets: uint64(senderStats.retransmitted),
			Quality:              float64(senderStats.quality),
			Rtt:                  uint32(senderStats.rtt),
		}
	} else if stats_container.stats_type == C.RIST_STATS_RECEIVER_FLOW {
		var receiverStats = (*C.struct_rist_stats_receiver_flow)(unsafe.Pointer(&stats_container.stats[0]))
		statsContainer.ReceiverFlowStats = &ReceiverFlowStats{
			PeerCount:                 uint32(receiverStats.peer_count),
			CName:                     C.GoString(&receiverStats.cname[0]),
			FlowID:                    uint32(receiverStats.flow_id),
			Status:                    int(receiverStats.status),
			Bandwidth:                 uint(receiverStats.bandwidth),
			RetryBandwidth:            uint(receiverStats.retry_bandwidth),
			SentPackets:               uint64(receiverStats.sent),
			ReceivedPackets:           uint64(receiverStats.received),
			MissingPackets:            uint32(receiverStats.missing),
			ReorderedPackets:          uint32(receiverStats.reordered),
			RecoveredPackets:          uint32(receiverStats.recovered),
			RecoveredFirstRetryReq:    uint32(receiverStats.recovered_one_retry),
			LostPackets:               uint32(receiverStats.lost),
			Quality:                   float64(receiverStats.quality),
			MinimalInterPacketArrival: uint64(receiverStats.min_inter_packet_spacing),
			CurrentInterPacketArrival: uint64(receiverStats.cur_inter_packet_spacing),
			MaximumInterPacketArrival: uint64(receiverStats.max_inter_packet_spacing),
			Rtt:                       uint32(receiverStats.rtt),
		}
	}
	userCb := gopointer.Restore(arg).(StatsCallbackFunc)
	go userCb(&statsContainer)
}

func SetStatsCallback(ctx RistCtx, statsInterval int, cbFunc StatsCallbackFunc) (unsafe.Pointer, error) {
	ptr := gopointer.Save(cbFunc)
	if res := C.rist_stats_callback_set(ctx, C.int(statsInterval), (*[0]byte)(C.ristStatsCB), ptr); res != 0 {
		return nil, fmt.Errorf("error setting stats callback: %d", res)
	}
	return ptr, nil
}

func StatsCallbackUnrefPtr(ptr unsafe.Pointer) {
	gopointer.Unref(ptr)
}
