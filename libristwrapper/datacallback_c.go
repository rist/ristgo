/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
 #include <librist/librist.h>

 extern void ristDataCallbackWrapper(void *arg, struct rist_data_block *data_block);

 int ristDataCallback(void *arg, struct rist_data_block *data_block)
 {
	 ristDataCallbackWrapper(arg, data_block);
	 return 0;
 }

*/
import "C"
