/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package libristwrapper

/*
#cgo pkg-config: librist
#include <librist/librist.h>


void _rist_receiver_data_read_wrapper(uintptr_t c,int timeout,uintptr_t block, uintptr_t res) {
	struct rist_ctx* ctx = (struct rist_ctx*)c;
	int *r = (int*) res;
	struct rist_data_block **b = (struct rist_data_block **)block;
	*r = rist_receiver_data_read2(ctx, b, timeout);
}
*/
import "C"
import (
	"fmt"
	"os"
	"unsafe"
)

type RistProfile int

const (
	RistProfileSimple RistProfile = 0
	RistProfileMain   RistProfile = 1
)

func ReceiverCreate(profile RistProfile, loggingSettings *RistLoggingSettings) (RistCtx, error) {
	var ctx *C.struct_rist_ctx
	if res := C.rist_receiver_create(&ctx, (C.enum_rist_profile)(profile), (*C.struct_rist_logging_settings)(loggingSettings)); res != 0 {
		return nil, fmt.Errorf("error creating rist receiver, error code: %d", res)
	}
	return RistCtx(ctx), nil
}

func ReceiverReadData(ctx RistCtx, timeout int) (*RistDataBlock, int, error) {
	res := C.int(0)
	var ristDataBlock *C.struct_rist_data_block
	c := uintptr(unsafe.Pointer(ctx))
	b := uintptr(unsafe.Pointer(&ristDataBlock))
	r := uintptr(unsafe.Pointer(&res))
	C._rist_receiver_data_read_wrapper(C.uintptr_t(c), C.int(timeout), C.uintptr_t(b), C.uintptr_t(r))
	if res < 0 {
		ret := res
		return nil, int(res), fmt.Errorf("read error: %d", ret)
	}
	if res == 0 || ristDataBlock == nil {
		return nil, 0, &RistDataReadTimeout{}
	}
	return wrapLibRistDataBlock(ristDataBlock), int(res), nil
}

func ReceiverSetNotifyFD(ctx RistCtx, write *os.File) error {
	res := C.rist_receiver_data_notify_fd_set(ctx, C.int(write.Fd()))
	if res != 0 {
		return fmt.Errorf("error setting rist data notify fd: %d", res)
	}
	return nil
}

func ReceiverStart(ctx RistCtx) error {
	res := C.rist_start(ctx)
	if res != 0 {
		return fmt.Errorf("error starting rist receiver %d", res)
	}
	return nil
}
