module code.videolan.org/rist/ristgo

go 1.14

require (
	github.com/Showmax/go-fqdn v1.0.0
	github.com/haivision/srtgo v0.0.0-20210308180300-b484f9267f13
	github.com/influxdata/influxdb-client-go/v2 v2.4.0
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/mattn/go-pointer v0.0.1
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/sys v0.0.0-20210611083646-a4fc73990273
	golang.org/x/tools v0.1.3 // indirect
)
