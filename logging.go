/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package ristgo

import (
	"fmt"
	"strings"
	"time"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

//LevelToString helper function to convert RistLogLevel to string
func LevelToString(loglevel libristwrapper.RistLogLevel) string {
	switch loglevel {
	case libristwrapper.LogLevelError:
		return "ERROR"
	case libristwrapper.LogLevelWarn:
		return "WARN"
	case libristwrapper.LogLevelNotice:
		return "NOTICE"
	case libristwrapper.LogLevelInfo:
		return "INFO"
	case libristwrapper.LogLevelDebug:
		return "DEBUG"
	default:
		return "UNKNOWN"
	}
}

//CreateDefaultLoggingCallback creates a callback that simply prints log messages to stdout
func CreateDefaultLoggingCallback(MaximalLogLevel libristwrapper.RistLogLevel) libristwrapper.LogCallbackFunc {
	return func(loglevel libristwrapper.RistLogLevel, logmessage string) {
		if loglevel <= MaximalLogLevel {
			timeNow := time.Now()
			fmt.Printf("[%s] [%s]: %s\n", timeNow.Format("2006-01-02T15:04:05-0700"), LevelToString(loglevel), strings.TrimSuffix(logmessage, "\n"))
		}
	}
}
