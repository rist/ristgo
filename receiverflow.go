/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package ristgo

import (
	"context"
	"errors"
	"time"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

//ReceiverFlow represents a demultiplexed stream.
type ReceiverFlow interface {
	Read(data []byte) (n int, err error)                   //Read data conforms to io.Reader
	ReadWithTimeout(timeout time.Duration) ([]byte, error) //ReadWithTimeout reads data with timeout
	DataChannel() <-chan *libristwrapper.RistDataBlock     //DataChannel returns a data channel for reading
	Destroy() error                                        //Destroy s the ReceiverFlow
}

type flow struct {
	destinationPort uint16
	lastSeq         uint32
	context         context.Context
	cancel          context.CancelFunc
	dataChannel     chan *libristwrapper.RistDataBlock
	receiver        *receiver
}

func (f *flow) Read(data []byte) (n int, err error) {
	b, err := f.ReadWithTimeout(-1)
	if err != nil {
		return 0, err
	}
	cn := copy(data, b)
	if cn < len(b) {
		return len(b), errors.New("receive buffer too small, lost some data")
	}
	return len(b), nil
}

func (f *flow) DataChannel() <-chan *libristwrapper.RistDataBlock {
	return f.dataChannel
}

func (f *flow) ReadWithTimeout(timeout time.Duration) ([]byte, error) {
	if timeout >= 0 {
		select {
		case <-f.context.Done():
			return nil, f.context.Err()
		case buf := <-f.dataChannel:
			return buf.Data, nil
		case <-time.After(timeout):
			return nil, errors.New("timeout exceeded")
		}
	}
	db := <-f.dataChannel
	return db.Data, f.context.Err()
}

func (f *flow) Destroy() error {
	f.cancel()
	close(f.dataChannel)
	f.receiver.flowRemove <- f.destinationPort
	return nil
}
