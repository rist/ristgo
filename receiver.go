/*
 * Copyright © 2021 ODMedia B.V.
 *
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package ristgo

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"runtime"
	"sync"
	"time"
	"unsafe"

	"code.videolan.org/rist/ristgo/libristwrapper"
)

//ReceiverConfig is the configuration used by ReceiverCreate
type ReceiverConfig struct {
	//RistProfile RIST profile to use for this session
	RistProfile libristwrapper.RistProfile
	//LoggingCallbackFunction is an optional logging callback function
	LoggingCallbackFunction libristwrapper.LogCallbackFunc
	//StatsCallbackFunction is an optional stats callback function
	StatsCallbackFunction libristwrapper.StatsCallbackFunc
	//StatsInterval is the interval in ms at which the StatsCallbackFunction will be called
	StatsInterval int
	//RecoveryBufferSize is the size in ms of the recovery buffer
	RecoveryBufferSize int
}

//Receiver is a rist receiver instance
type Receiver interface {
	//Start starts the rist receiver
	Start() error
	//AddPeer adds a new connection to the rist instance
	AddPeer(config *PeerConfig) (int, error)
	//RemovePeer removes a connection from the rist session
	RemovePeer(id int) error
	//ConfigureFlow sets up a Flow to read data from
	ConfigureFlow(destinationPort uint16) (ReceiverFlow, error)
	//Destroy stops the rist session and destroys all associated resources.
	Destroy()
}

//ReceiverCreate creates a new Receiver
func ReceiverCreate(c context.Context, config *ReceiverConfig) (Receiver, error) {
	var (
		err             error                               = nil
		ristCtx         libristwrapper.RistCtx              = nil
		loggingSettings *libristwrapper.RistLoggingSettings = nil
	)

	if config.LoggingCallbackFunction != nil {
		loggingSettings = libristwrapper.CreateRistLoggingSettingsWithCB(config.LoggingCallbackFunction)
	}
	defer func() {
		if err != nil {
			libristwrapper.CleanupLoggingSettings(loggingSettings)
		}
	}()

	ristCtx, err = libristwrapper.ReceiverCreate(config.RistProfile, loggingSettings)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			libristwrapper.RistDestroy(ristCtx)
		}
	}()

	var statsCBPtr unsafe.Pointer = nil
	if config.StatsCallbackFunction != nil {
		statsCBPtr, err = libristwrapper.SetStatsCallback(ristCtx, config.StatsInterval, config.StatsCallbackFunction)
		if err != nil {
			return nil, err
		}
	}

	receiverContext, cancelFunc := context.WithCancel(c)
	return &receiver{
		context:            receiverContext,
		cancel:             cancelFunc,
		ristCtx:            ristCtx,
		profile:            config.RistProfile,
		loggingSettings:    loggingSettings,
		recoveryBufferSize: config.RecoveryBufferSize,
		statsCBPtr:         statsCBPtr,
		logCB:              config.LoggingCallbackFunction,
		flows:              make(map[uint16]*flow),
		flowadd:            make(chan *flowadd),
		flowRemove:         make(chan uint16),
	}, nil
}

type receiver struct {
	context            context.Context
	cancel             context.CancelFunc
	ristCtx            libristwrapper.RistCtx
	profile            libristwrapper.RistProfile
	loggingSettings    *libristwrapper.RistLoggingSettings
	statsCBPtr         unsafe.Pointer
	recoveryBufferSize int
	peers              peers
	flows              map[uint16]*flow
	logCB              libristwrapper.LogCallbackFunc
	flowadd            chan *flowadd
	flowRemove         chan uint16
	wg                 sync.WaitGroup
}

type flowadd struct {
	destinationPort uint16
	returnChannel   chan ReceiverFlow
	err             chan error
}

func (r *receiver) Start() error {
	ec := make(chan error)
	go r.receiverLoop(ec)
	err := <-ec
	r.wg.Add(1)
	return err
}

func (r *receiver) AddPeer(config *PeerConfig) (int, error) {
	return r.peers.addPeer(r.ristCtx, config, uint32(r.recoveryBufferSize))
}

func (r *receiver) RemovePeer(id int) error {
	return r.peers.removePeer(id)
}

func (r *receiver) ConfigureFlow(destinationPort uint16) (ReceiverFlow, error) {
	if r.profile == libristwrapper.RistProfileSimple && destinationPort != 0 {
		return nil, fmt.Errorf("destinationport %d illegal when using simple profile", destinationPort)
	}

	flowadd := &flowadd{
		destinationPort,
		make(chan ReceiverFlow),
		make(chan error),
	}
	r.flowadd <- flowadd
	err := <-flowadd.err
	if err != nil {
		return nil, err
	}
	return <-flowadd.returnChannel, nil
}

func (r *receiver) Destroy() {
	r.cancel()
	r.wg.Wait()
}

func (r *receiver) logViaCb(level libristwrapper.RistLogLevel, format string, arg ...interface{}) {
	msg := fmt.Sprintf(format, arg...)
	if r.logCB != nil {
		r.logCB(level, msg)
	}
}

func (r *receiver) handleLibRistDataBlock(b *libristwrapper.RistDataBlock) error {
	destinationPort := b.DstPort
	if r.profile == libristwrapper.RistProfileSimple {
		destinationPort = 0
	}
	if f, ok := r.flows[destinationPort]; ok {
		expectedSeq := (f.lastSeq + 1)
		f.lastSeq = b.SeqNo

		//extended seq is 32 bits, normal sequence number is 16, so cast to 16 and back to 32 to force wrapping
		if !b.ExtendedSeq {
			expectedSeq = uint32(uint16(expectedSeq))
		}
		if expectedSeq != b.SeqNo {
			b.Discontinuity = true
		}
		select {
		case f.dataChannel <- b:
			//
		default:
			return errors.New("couldn't write to flow")
		}
	}
	return nil
}

func (r *receiver) receiverLoop(ec chan error) {
	notifyBuf := make([]byte, 1)
	reader, writer, err := os.Pipe()
	if err != nil {
		ec <- err
		goto cleanup
	}
	err = libristwrapper.ReceiverSetNotifyFD(r.ristCtx, writer)
	if err != nil {
		ec <- err
		goto cleanup
	}
	err = libristwrapper.ReceiverStart(r.ristCtx)
	if err != nil {
		ec <- err
		goto cleanup
	}
	ec <- nil
	reader.SetReadDeadline(time.Now().Add(time.Duration(200) * time.Millisecond))
main:
	for {
		_, err := reader.Read(notifyBuf)
		//Happy path, err == nil, written like this so the select statement further down
		//is reached without goto's
		if err == nil {
			//completely drain the fifo whenever we get a chance to read it, to prevent ever
			//falling back.
			for {
				b, _, err := libristwrapper.ReceiverReadData(r.ristCtx, 0)
				if err != nil || b == nil {
					if errors.Is(err, &libristwrapper.RistDataReadTimeout{}) {
						break
					}
					r.logViaCb(libristwrapper.LogLevelError, "error receiving data: %s\n", err)
					goto cleanup
				}
				if err = r.handleLibRistDataBlock(b); err != nil {
					r.logViaCb(libristwrapper.LogLevelError, "error handling datablock %s\n", err)
				}
			}
		} else if errors.Is(err, os.ErrDeadlineExceeded) {
			//rearm deadline timer
			reader.SetReadDeadline(time.Now().Add(time.Duration(200) * time.Millisecond))
		} else if err != nil && err != io.EOF && !errors.Is(err, os.ErrDeadlineExceeded) {
			r.logViaCb(libristwrapper.LogLevelError, "error reading from notify buf: %s\n", err)
			goto cleanup
		}
		select {
		case <-r.context.Done():
			break main
		case add := <-r.flowadd:
			if _, ok := r.flows[add.destinationPort]; !ok {
				context, cancel := context.WithCancel(r.context)
				add.err <- nil
				flow := flow{
					destinationPort: add.destinationPort,
					context:         context,
					cancel:          cancel,
					dataChannel:     make(chan *libristwrapper.RistDataBlock, 256),
					receiver:        r,
				}
				r.flows[add.destinationPort] = &flow
				add.returnChannel <- &flow
			} else {
				add.err <- fmt.Errorf("error configuring flow, pre-existing flow with destination port %d already configured", add.destinationPort)
			}
		case remove := <-r.flowRemove:
			delete(r.flows, remove)
		default:
			//nothing
		}
	}
	//Otherwise the FD get's closed
	runtime.KeepAlive(writer)
cleanup:
	close(r.flowadd)
	close(r.flowRemove)
	libristwrapper.RistDestroy(r.ristCtx)

	r.logViaCb(libristwrapper.LogLevelInfo, "starting cleanup")
	for key := range r.flows {
		delete(r.flows, key)
	}

	libristwrapper.RistDestroy(r.ristCtx)
	r.ristCtx = nil

	libristwrapper.CleanupLoggingSettings(r.loggingSettings)
	libristwrapper.StatsCallbackUnrefPtr(r.statsCBPtr)
	r.wg.Done()
}
